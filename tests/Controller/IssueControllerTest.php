<?php

namespace App\Tests\Controller;

use App\Entity\Issue;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class IssueControllerTest extends WebTestCase
{
    /** @var \Symfony\Bundle\FrameworkBundle\KernelBrowser $client */
    private $client;

    protected function setUp(): void
    {
        $this->client = $this->client ?? static::createClient();
    }

    public function testWorkflowNotRespected(){
        $this->client->request('GET', 'issue/1/edit');

        $crawler = $this->client->submitForm('Update', [
            'issue[state]' => Issue::RESOLVED_STATE
        ]);

        $this->assertResponseStatusCodeSame(500);
    }

    public function testEdit()
    {
        $this->client->request('GET', 'issue/1/edit');

        $crawler = $this->client->submitForm('Update', [
            'issue[state]' => Issue::WONT_FIX_STATE
        ]);

        $this->assertResponseRedirects();
        $this->assertResponseStatusCodeSame(303);
    }
}
