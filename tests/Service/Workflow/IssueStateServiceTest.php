<?php

namespace App\Tests\Service\Workflow;

use App\Entity\Issue;
use App\Service\Workflow\IssueStateService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Workflow\Registry;
use Symfony\Component\Workflow\Workflow;

class IssueStateServiceTest extends TestCase
{
    /** @var Registry $registryMock */
    private $registryMock;
    /** @var IssueStateService $issueStateService */
    private $issueStateService;

    protected function setUp(): void
    {
        $this->registryMock = $this->getMockBuilder(Registry::class)
            ->getMock();

        $this->issueStateService = new IssueStateService($this->registryMock);
    }

    /**
     * @param $initialState
     * @param $newState
     * @param $expected
     *
     * @dataProvider changeStateDataProvider
     */
    public function testChangeStateIsValid(
        Issue $oldIssueMock,
        Issue $newIssueMock,
              $resultCan,
              $expected
    )
    {
        $workflow = $this->getMockBuilder(Workflow::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->registryMock->expects(self::atMost(1))
            ->method('get')
            ->willReturn($workflow);

        $workflow->expects(self::atMost(1))
            ->method('can')
            ->willReturn($resultCan);

        $res = $this->issueStateService->changeStateIsValid($oldIssueMock, $newIssueMock);

        $this->assertEquals($expected, $res);
    }

    /**
     * @return array
     */
    public function changeStateDataProvider()
    {
        $newIssue = $this->buildIssue(Issue::NEW_STATE);
        $inProgressIssue = $this->buildIssue(Issue::IN_PROGRESS_STATE);
        $resolvedIssue = $this->buildIssue(Issue::RESOLVED_STATE);

        return [
            [$newIssue, $inProgressIssue, true, true],
            [$newIssue, $newIssue, true, true],
            [$newIssue, $newIssue, false, true],
            [$newIssue, $resolvedIssue, false, false]
        ];
    }

    /**
     * @param $state
     * @return Issue
     */
    private function buildIssue($state)
    {
        $issue = new Issue();
        $issue->setState($state);
        return $issue;
    }
}
