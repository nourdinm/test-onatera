vm_name           = "onatera.test"       # VM Name
vm_ram            = "2096"                # VM RAM
vm_cpu            = 2                     # VM CPU
db_login          = "root"                # Database login
db_password       = "root"                # Database password
githubtoken       = ""                    # Your github token (optional)

Vagrant.configure("2") do |config|

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the hxchost machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL

  config.vm.define vm_name

  config.vm.box = "generic/ubuntu2004"
  config.vm.box_download_insecure = true
  config.vm.box_check_update = true

  config.vm.network :forwarded_port, host: 80, guest: 80, id: "http", auto_correct: true
  config.vm.network :forwarded_port, host: 8080, guest: 8080, id: "http2", auto_correct: true
  config.vm.network "forwarded_port", host: 3306, guest: 3306, id: "mysql", auto_correct: true
  config.vm.network :forwarded_port, host: 6379, guest: 6379, id: "redis", auto_correct: true

  config.vm.provider "virtualbox" do |v|
      #v.gui = true
      v.cpus = vm_cpu
      v.customize ["modifyvm", :id, "--memory", vm_ram]
      v.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/var/www/onatera/", "1"]
      v.customize ["modifyvm", :id, "--cableconnected1", "on"]
      v.customize ['modifyvm', :id, '--clipboard', 'bidirectional']
      v.customize ["modifyvm", :id, "--vram", 64]
      v.customize [ "modifyvm", :id, "--uartmode1", "disconnected" ]
    end
  
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.synced_folder ".", "/var/www/onatera" , owner: "vagrant", group: "www-data", mount_options: ['dmode=775', 'fmode=775']

  config.vm.provision "shell", inline: "sudo apt-get update"
  #config.vm.provision "shell", inline: "sudo apt-get install -y python3-pip"

  #install ansible
  #config.vm.provision "shell", inline: "sudo apt install software-properties-common"
  #config.vm.provision "shell", inline: "sudo apt-add-repository --yes --update ppa:ansible/ansible"
  config.vm.provision "shell", inline: "sudo apt install --yes ansible"
  config.vm.provision "shell", inline: "sudo apt install --yes openssl"

  #in case cannot add ondrej/php repository
  #sudo vim /etc/apt/sources.list
  #Add at the end of line
  #deb http://ppa.launchpad.net/ondrej/php/ubuntu bionic main
  #deb-src http://ppa.launchpad.net/ondrej/php/ubuntu bionic main
  #sudo apt-get update
  #sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4F4EA0AAE5267A6C
  #sudo apt-get update

  config.vm.provision "ansible_local" do |ansible|
     ansible.install_mode    = "pip3"
     ansible.provisioning_path = "/var/www/onatera"
     ansible.playbook        = ".provisioning/playbook.yml"
     ansible.config_file     = ".provisioning/ansible.cfg"
     ansible.verbose         = false
     ansible.install         = true
  end

end
