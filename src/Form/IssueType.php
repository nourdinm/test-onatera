<?php

namespace App\Form;

use App\Entity\Issue;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IssueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title')
            ->add('description')
            ->add('priority', ChoiceType::class, [
                'choices' => [
                    'Low' => Issue::LOW_PRIORITY,
                    'Normal' => Issue::NORMAL_PRIORITY,
                    'Urgent' => Issue::URGENT_PRIORITY,
                    'Critical' => Issue::CRITICAL_PRIORITY
                ]
            ])
            ->add('state', ChoiceType::class, [
                'choices' => [
                    'New' => Issue::NEW_STATE,
                    'In progress' => Issue::IN_PROGRESS_STATE,
                    'Wont fix' => Issue::WONT_FIX_STATE,
                    'Resolved' => Issue::RESOLVED_STATE
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Issue::class,
        ]);
    }
}
