<?php

namespace App\DataFixtures;

use App\Entity\Issue;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $issue = new Issue();
        $issue->setTitle("Issue 1");
        $issue->setDescription("Description issue");
        $issue->setState(Issue::NEW_STATE);
        $issue->setPriority(Issue::NORMAL_PRIORITY);
        $issue->setId(1);
        $metadata = $manager->getClassMetadata(get_class($issue));
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);

        $manager->persist($issue);

        $manager->flush();
    }
}
