<?php

namespace App\Controller;

use App\Entity\Issue;
use App\Form\IssueType;
use App\Repository\IssueRepository;
use App\Service\Workflow\IssueStateService;
use Doctrine\Bundle\DoctrineBundle\DataCollector\DoctrineDataCollector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/issue")
 */
class IssueController extends AbstractController
{
    /** @var IssueStateService $issueStateService */
    private $issueStateService;

    /**
     * @param IssueStateService $issueStateService
     */
    public function __construct(IssueStateService $issueStateService)
    {
        $this->issueStateService = $issueStateService;
    }

    /**
     * @Route("/", name="issue_index", methods={"GET"})
     */
    public function index(IssueRepository $issueRepository): Response
    {
        return $this->render('issue/index.html.twig', [
            'issues' => $issueRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="issue_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $issue = new Issue();
        $form = $this->createForm(IssueType::class, $issue);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($issue);
            $entityManager->flush();

            return $this->redirectToRoute('issue_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('issue/new.html.twig', [
            'issue' => $issue,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="issue_show", methods={"GET"})
     */
    public function show(Issue $issue): Response
    {
        return $this->render('issue/show.html.twig', [
            'issue' => $issue,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="issue_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Issue $issue, EntityManagerInterface $entityManager): Response
    {
        $oldIssue = clone $issue;
        $form = $this->createForm(IssueType::class, $issue);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if(!$this->issueStateService->changeStateIsValid($oldIssue, $issue)){
                throw new \LogicException("Workflow have not been respected.");
            }
            $entityManager->flush();

            return $this->redirectToRoute('issue_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('issue/edit.html.twig', [
            'issue' => $issue,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="issue_delete", methods={"POST"})
     */
    public function delete(Request $request, Issue $issue, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$issue->getId(), $request->request->get('_token'))) {
            $entityManager->remove($issue);
            $entityManager->flush();
        }

        return $this->redirectToRoute('issue_index', [], Response::HTTP_SEE_OTHER);
    }
}
