<?php

namespace App\Service\Workflow;

use App\Entity\Issue;
use Symfony\Component\Workflow\Registry;

class IssueStateService
{
    const TRANSITIONS = [
        Issue::IN_PROGRESS_STATE => "to_inprogress",
        Issue::WONT_FIX_STATE => "to_wontfix",
        Issue::RESOLVED_STATE => "to_resolved"
    ];

    /** @var Registry $registry */
    private $registry;

    public function __construct(
        Registry $registry
    )
    {
        $this->registry = $registry;
    }

    /**
     * @param Issue $old
     * @param Issue $new
     * @return bool
     */
    public function changeStateIsValid(Issue $old, Issue $new): bool
    {
        $valid = false;

        if ($old->getState() === $new->getState() ||
            $this->canTransit($old, $new->getState())
        ) {
            $valid = true;
        }

        return $valid;
    }

    /**
     * @param Issue $issue
     * @param string $newState
     * @return bool
     */
    private function canTransit(Issue $issue, string $newState): bool
    {
        if (!array_key_exists($newState, self::TRANSITIONS)) {
            return false;
        }

        $workflow = $this->registry->get($issue, 'issue_status');
        $transition = self::TRANSITIONS[$newState];

        return $workflow->can($issue, $transition);
    }
}
