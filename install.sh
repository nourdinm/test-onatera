#!/bin/bash

composer install
php bin/console doctrine:schema:update -f
php bin/console --env=test doctrine:schema:update --force
php bin/console --env=test doctrine:fixtures:load
