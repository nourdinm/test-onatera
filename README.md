# Test Onatera


## Require
These following software are mandatory:
- Vagrant [link here](https://www.vagrantup.com/downloads)
- Virtualbox [link here](https://www.virtualbox.org/wiki/Downloads)

## Install the project
N.B.: Open a terminal in project path or use phpstorm menu (Tools > Vagrant )

1/ Install VM: Execute command 
```
vagrant up
```
2/ Install project dependencies
```
vagrant shh
cd /var/www/onatera
./install.sh
```

3/ Edit windows hosts file to access to website add (C:/Windows/System32/drivers/etc/hosts)
```
127.0.0.1	onatera.test
```

## Application tests

1/ Run

```
php ./vendor/bin/phpunit
```

2/ Load fixtures

```
php bin/console --env=test doctrine:fixtures:load
```